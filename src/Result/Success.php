<?php

declare(strict_types=1);

namespace SunnyFlail\SimpleTerminal\Result;

final class Success extends AbstractResult implements SuccessInterface
{
    public function isSuccessful(): bool
    {
        return true;
    }
}
