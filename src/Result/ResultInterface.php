<?php

declare(strict_types=1);

namespace SunnyFlail\SimpleTerminal\Result;

interface ResultInterface
{
    public function getCode(): int;

    public function getOutput(): string;

    public function isSuccessful(): bool;
}
