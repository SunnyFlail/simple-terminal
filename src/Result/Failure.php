<?php

declare(strict_types=1);

namespace SunnyFlail\SimpleTerminal\Result;

final class Failure extends AbstractResult implements FailureInterface
{
    public function isSuccessful(): bool
    {
        return false;
    }
}
