<?php

declare(strict_types=1);

namespace SunnyFlail\SimpleTerminal\Result;

interface FailureInterface extends ResultInterface
{
}
