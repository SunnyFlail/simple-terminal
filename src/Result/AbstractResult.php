<?php

declare(strict_types=1);

namespace SunnyFlail\SimpleTerminal\Result;

abstract class AbstractResult implements ResultInterface
{
    public function __construct(protected string $output, protected int $code)
    {
    }

    public function getOutput(): string
    {
        return $this->output;
    }

    public function getCode(): int
    {
        return $this->code;
    }
}
