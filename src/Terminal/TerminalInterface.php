<?php

declare(strict_types=1);

namespace SunnyFlail\SimpleTerminal\Terminal;

use SunnyFlail\SimpleTerminal\Result\SuccessInterface;
use SunnyFlail\SimpleTerminal\Result\FailureInterface;

interface TerminalInterface
{
    public function execute(string $command): SuccessInterface|FailureInterface;
}
