<?php

declare(strict_types=1);

namespace SunnyFlail\SimpleTerminal\Terminal;

use SunnyFlail\SimpleTerminal\Result\Failure;
use SunnyFlail\SimpleTerminal\Result\FailureInterface;
use SunnyFlail\SimpleTerminal\Result\Success;
use SunnyFlail\SimpleTerminal\Result\SuccessInterface;

final class VanillaTerminal implements TerminalInterface
{
    public function execute(string $command): SuccessInterface|FailureInterface
    {
        exec($command, $output, $resultCode);
        $output = implode(PHP_EOL, $output);

        if ($resultCode === 0) {
            return new Success($output, $resultCode);
        }

        return new Failure($output, $resultCode);
    }
}
